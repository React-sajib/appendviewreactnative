//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions,Image, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'
let width = Dimensions.get('window').width; //full width
let height = Dimensions.get('window').height; //full height
import moment from 'moment'
// create a component
class TodoList extends Component {
  is_mounted = false
  constructor(props){
    super(props)
    this.state = {
      date:'',
      text:'',
      newTexts:[],
      NewArry:[],
      AppendForms:[
        {
          name:'',
          mobile:''
        }
      ]
    }
  }

  componentDidMount = ()=>{
    this.is_mounted = true
    // this.GetDesignView()
  }

  componentWillUnmount = ()=>{
    this.is_mounted=false
    
  }

  Increment = (event)=>{
    if(this.state.AppendForms.length>=3){
      alert('you cannot growUp more')
   }else{
       this.setState({
           AppendForms: this.state.AppendForms.concat([{ name: '' , mobile : '' }])
       })
   }
  }

  Decrement = (event)=>{
      this.setState({
        AppendForms: this.state.AppendForms.filter((s, sidx) =>{
          if(this.state.AppendForms.length > 1){
              return (this.state.AppendForms.length - 1) !== sidx
          }else{
              return this.state.AppendForms.concat([{ name: '' , mobile : '' }])
          }
      })
    })
   
  }

  ChangeText = (chk,e,id)=>{
     if(chk !== 'name'){
      const NewAppendForms =this.state.AppendForms.map((data,i)=>{
          if(id !== i) return data 
          return {...data, mobile:e }
      })
      this.setState({ AppendForms: NewAppendForms });
     }else{
      const NewAppendForms = this.state.AppendForms.map((data,i)=>{
        if(id !== i) return data 
        return {...data, name:e }
      })
        this.setState({ AppendForms: NewAppendForms });
     }
  }


 

  render() {
    let today = moment().format('MMMM Do YYYY');
      return (
        <View style={styles.container}>
          <View style={{ position: 'relative' }}>
            <View style={{ height:(height/3)*1, }}>
              <Image
                source={require('./assets/Shopping.jpg')}
                style={{ height:'100%', width:'100%', }}
              />
            </View>
            <View style={{ position:'absolute',bottom:10,left:0,paddingHorizontal:15,paddingTop:10,paddingBottom:10,backgroundColor:'rgba(0,0,0,.4)' }}>
              <TouchableOpacity onPress={()=>console.warn('retweet')}>
                <Icon
                  name="retweet"
                  style={{ color:'#fff',fontSize:25, }}
                />
              </TouchableOpacity>
            </View>
            <View style={{ position:'absolute',top:10,right:0,paddingHorizontal:15,paddingTop:10,paddingBottom:10,backgroundColor:'rgba(0,0,0,.4)' }}>
              <Text style={{ color:'#fff',fontWeight:'bold',fontSize:20 }}>{today}</Text>
            </View>
          </View>
          <View style={{ justifyContent:'center',bottom:0,left:0,width:'100%',paddingHorizontal:15,paddingVertical:15}}>
              <View style={{ flexDirection:'row' }}>
                <View style={{  }}>
                  <TouchableOpacity onPress={()=>this.Increment()}>
                    <Icon
                      name="plus-circle"
                      style={{ color:'#000',fontSize:35,justifyContent:'center',marginTop:8,marginRight:5 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.Decrement()}>
                    <Icon
                      name="minus-circle"
                      style={{ color:'#000',fontSize:35,justifyContent:'center',marginTop:8,marginRight:5 }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ flex:3 }}>
                {
                   this.state.AppendForms.map((data,i)=>{
                    return  <View key={i}>
                                <View style={{ marginBottom:10 }}>
                                  <TextInput 
                                    placeholder="Enter name"
                                    onChangeText={(e)=>this.ChangeText('name',e,i)}
                                    placeholderTextColor="#000"
                                    value={data.name}
                                    style={{ color:'#000',borderColor:'#000',borderWidth:1,borderRadius:10 }}
                                  />
                                </View>
                                <View style={{ marginBottom:10 }}>
                                  <TextInput 
                                    placeholder="Enter number"
                                    onChangeText={(e)=>this.ChangeText('mobile',e,i)}
                                    placeholderTextColor="#000"
                                    value={data.mobile}
                                    style={{ color:'#000',borderColor:'#000',borderWidth:1,borderRadius:10 }}
                                  />
                                </View>
                            </View>
                })
                }
                </View>
              </View>
          </View>
        </View>
      )
    }
    
  }


// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default TodoList;
